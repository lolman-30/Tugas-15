@extends('welcome')

@section('content')
    <h2>Detail Pemain Film</h2>

    <table class="table mt-3">
        <tr>
            <th>ID</th>
            <td>{{ $cast->id }}</td>
        </tr>
        <tr>
            <th>Nama</th>
            <td>{{ $cast->nama }}</td>
        </tr>
        <tr>
            <th>Umur</th>
            <td>{{ $cast->umur }}</td>
        </tr>
        <tr>
            <th>Bio</th>
            <td>{{ $cast->bio }}</td>
        </tr>
    </table>

    <a href="{{ url('/cast') }}" class="btn btn-secondary">Kembali</a>
@endsection
