@extends('welcome')

@section('content')
    <h2>List Data Pemain Film</h2>

    <a href="{{ url('/cast/create') }}" class="btn btn-primary">Tambah Pemain Film</a>

    <table class="table mt-3">
        <thead>
            <tr>
                <th>ID</th>
                <th>Nama</th>
                <th>Umur</th>
                <th>Bio</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($casts as $cast)
                <tr>
                    <td>{{ $cast->id }}</td>
                    <td>{{ $cast->nama }}</td>
                    <td>{{ $cast->umur }}</td>
                    <td>{{ $cast->bio }}</td>
                    <td>
                        <a href="{{ url('/cast/'.$cast->id) }}" class="btn btn-info btn-sm">Detail</a>
                        <a href="{{ url('/cast/'.$cast->id.'/edit') }}" class="btn btn-warning btn-sm">Edit</a>
                        <form action="{{ url('/cast/'.$cast->id) }}" method="POST" style="display:inline;">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Yakin ingin menghapus?')">Hapus</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection
