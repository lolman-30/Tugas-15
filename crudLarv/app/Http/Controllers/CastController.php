<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Cast;

class CastController extends Controller
{
    public function index()
    {
        $casts = Cast::all();
        return view('casts.index', compact('casts'));
    }

    public function create()
    {
        return view('casts.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|max:50',
            'umur' => 'required|integer',
            'bio' => 'required',
        ]);

        Cast::create($request->all());

        return redirect('/cast')->with('success', 'Data pemain film berhasil disimpan!');
    }

    public function show($cast_id)
    {
        $cast = Cast::find($cast_id);

        if (!$cast) {
            return abort(404, 'Cast tidak ditemukan!');
        }

        return view('casts.show', ['cast' => $cast]);
    }

    public function edit($cast_id)
    {
        $cast = Cast::find($cast_id);

        if (!$cast) {
            return abort(404, 'Cast tidak ditemukan!');
        }

        return view('casts.edit', ['cast' => $cast]);
    }

    public function update(Request $request, $cast_id)
    {
        $request->validate([
            'nama' => 'required|max:50',
            'umur' => 'required|integer',
            'bio' => 'required',
        ]);

        $cast = Cast::find($cast_id);

        $cast->update([
            'nama' => $request->input('nama'),
            'umur' => $request->input('umur'),
            'bio' => $request->input('bio'),
        ]);

        return redirect('/cast')->with('success', 'Data pemain film berhasil diperbaruin!');
    }

    public function destroy($cast_id)
    {
        $cast = Cast::findOrFail($cast_id);
        $cast->delete();

        return redirect('/cast')->with('success', 'Data pemain film berhasil dihapus!');
    }
}